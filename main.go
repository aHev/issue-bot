package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)
import "encoding/json"


func main() {
	log.Print("Starting Post Printer...")
	defer log.Print("Closing Post Printer.")

	port := os.Getenv("PORT")

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		log.Printf("Request: %+v", r)
		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Printf("Error reading body: %s", err)
			return
		}

		log.Printf("Body: %s", data)

		var jsonMap map[string]interface{}
		json.Unmarshal([]byte(data), &jsonMap)

		if r.Method == "POST" {
			w.WriteHeader(http.StatusCreated)
			return
		}

		w.WriteHeader(http.StatusOK)
	})

	http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
}
