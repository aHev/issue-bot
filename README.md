# issue-bot

## Summary
This function will listen for new issues and pefrorm various action once 
received. Currently this is supporting the following actions.

* Post comment to issue

## Approach 
This function will listen for HTTP events that post a JSON body that 
contains details about the project that the issue was filed on and the comment 
on that issue. To do this it will need to extract the following from the JSON
recieved from the 
[webhook api](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#issues-events)

 * `object_attributes.id`
 * `object_attributes.iid`
  
Once those items are extracted the function will construct a JSON payload to be 
posted to the [comments API](https://docs.gitlab.com/ee/api/discussions.html#add-note-to-existing-issue-discussion)
based on the following mapping from the webook. 

 * `object_attributes.id` - `id`
 * `object_attributes.iid` - `issue_iid`
 * `discussion.id` - `not sure where to get this - maybe 0`
 * `note.id` - `not sure where to get this - maybe 0`
 * `body` - "Thanks for your contribution!"
 
## Deploying as a function
This app is an ideal candidate to be run in a serevrless context for
where the instance is scaled to 0. The simplest approach to this can
be accomplished with Google App Engine using the included app.yaml

Simply run the following command.
```
gcloud app deploy
```

## Future action ideas
To make this app more functional the following enhancments are recommended.
 * [Add working hours](https://gitlab.com/aHev/issue-bot/issues/5)
 * [Add slack integration](https://gitlab.com/aHev/issue-bot/issues/6)
 * [Configurable messages](https://gitlab.com/aHev/issue-bot/issues/7)